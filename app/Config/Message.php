<?php

namespace App\Config;

class Message
{
	const ORDER_NOT_FOUND 			= 'Order not found';
	const ORDER_WRONG_STATUS 		= 'Order cannot be paid again';
	const ORDER_WRONG_USER 			= 'You don\'t own this user';
	const ORDER_ALREADY_CANCELED 	= 'Order already canceled';
}