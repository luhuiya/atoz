<?php

namespace App\Config;

class OrderStatus
{
	const PENDING 	= 'pending';
	const SUCCESS 	= 'success';
	const FAIL 		= 'fail';
	const CANCELED 	= 'canceled';
}