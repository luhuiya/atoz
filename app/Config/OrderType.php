<?php

namespace App\Config;

class OrderType
{
	const PREPAID_BALANCE = 'prepaid_balance';
	const PRODUCT_COMMERCE = 'product_commerce';
}