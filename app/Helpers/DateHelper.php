<?php

namespace App\Helpers;

use Carbon\Carbon;

class DateHelper
{
	static function isDaylight()
	{
		//daylight (9AM to 5PM) 
		$now = Carbon::now();
		$date1 = Carbon::create($now->year, $now->month, $now->day, 9, 0, 0);
		$date2 = Carbon::create($now->year, $now->month, $now->day, 17, 0, 0);

		return $now->between($date1, $date2, true);
	}
}
