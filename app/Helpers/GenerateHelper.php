<?php

namespace App\Helpers;

class GenerateHelper
{
	static function getIsSuccess($isDaylight)
	{
		// If the paid time happened in the daylight (9AM to 5PM) there is 90% success rate for inserting the balance, 40% otherwise. 
		$daylight 	= [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 0];
		$night 		= [ 1, 1, 1, 1, 0, 0, 0, 0, 0, 0];
		$rand = rand(0, 9);

		return $isDaylight? $daylight[$rand] : $night[$rand];
	}

	static function getRandomString($size = 8)
	{
		$arr = range('A', 'Z');
		$str = '';
		for ($i=0; $i<$size; $i++)
		{
			$rand = rand(0, 25);
			$str .= $arr[$rand];
		}
		return $str;
	}

	static function getRandomNumber($size = 10)
	{
		$str = '';
		for ($i=0; $i<$size; $i++)
			$str .= rand(0, 9);
		return $str;
	}

}
