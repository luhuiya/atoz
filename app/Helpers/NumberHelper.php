<?php

namespace App\Helpers;

class NumberHelper
{
	static function format($value)
	{
		return number_format($value, 0, ',', '.');
	}
}