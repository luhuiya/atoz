<?php

namespace App\Helpers;

use App\Config\OrderConfig;
use Carbon\Carbon;

class OrderHelper
{
	static function isExpired($timestamp)
	{
		$now = Carbon::now();
		$diff = $now->diffInSeconds($timestamp);
		return $diff >= OrderConfig::EXPIRED_TIME_SECONDS;
	}
}
