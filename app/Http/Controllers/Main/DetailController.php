<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use Validator;

abstract class DetailController extends UserController
{
    protected $paramsRules = [];
    protected $formView = '';
    protected $formUrl = '';
    protected $service;

    public function index()
    {
        return view($this->formView);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $rules = $this->paramsRules;
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails())
        {
            return redirect()->route($this->formUrl)->withErrors($validator)->withInput();
        }

        $orderId = $this->service->createOrder($input);
        return redirect()->route('success' , [ 'id' => $orderId ] );
    }
}
