<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;

use App\Services\ListOrderService;
use Illuminate\Support\Facades\Input;

class OrderController extends UserController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = Input::get('q');
        if(!empty($search))
        {
            if(strlen($search) > 10 || !is_numeric($search))
            {
                return redirect()->route('order');
            }
        }

    	$instance = new ListOrderService();
    	$data = $instance->get($search);
    	
        // $search
        return view('main.order')
            ->with('data', $data)
            ->with('search', $search);
    }
}
