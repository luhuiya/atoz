<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use Validator;

use App\Http\Controllers\UserController;
use App\Services\PaymentService;

class PaymentController extends UserController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($number = '')
    {
        return view('main.payment')->with('number', $number);
    }

    private function fail($validator)
    {
        return redirect()->route('payment.form')->withErrors($validator)->withInput();
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $rules = [ 'order_number' => 'required|digits:10' ];
        $validator = Validator::make($input, $rules);

        if ($validator->fails())
        {
            return $this->fail($validator);   
        }

        $service = new PaymentService();
        $result = $service->pay($input['order_number']);
        if(isset($result['error']))
        {
            $validator->getMessageBag()->add('order_number', $result['error']);
            return $this->fail($validator);   
        }

        return redirect()->route('order');
    }

}
