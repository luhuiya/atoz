<?php

namespace App\Http\Controllers\Main;

use App\Services\PrepaidService;

class PrepaidController extends DetailController
{
    protected $paramsRules = [
        'prepaid_number' => 'required|digits_between:7,12|regex:/(081)[0-9]{4,9}/',
        'prepaid_value' => 'required|in:10000,50000,100000',
    ];
    protected $formView = 'main.prepaid';
    protected $formUrl = 'prepaid.form';
    protected $service;

    public function __construct()
    {
    	$this->service = new PrepaidService();
    }
}
