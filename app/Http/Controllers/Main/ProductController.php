<?php

namespace App\Http\Controllers\Main;

use App\Services\ProductService;

class ProductController extends DetailController
{
    protected $paramsRules = [
        'product_name' => 'required|min:10|max:150',
        'product_shipping_address' => 'required|min:10|max:150',
        'product_price' => 'required|numeric',
    ];
    protected $formView = 'main.product';
    protected $formUrl = 'product.form';
    protected $service;

    public function __construct()
    {
        $this->service = new ProductService();
    }
}
