<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;

use App\Services\SuccessService;

class SuccessController extends UserController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($number = '')
    {
        if(empty($number) || !is_numeric($number))
        {
            abort(404);
        }

        $instance = new SuccessService();
        $data = $instance->getData($number);
        if(empty($data))
        {
            abort(404);
        }

        return view('main.success')->with('data', $data);
    }
}
