<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogOrderStatusModel extends Model
{
    protected $table = 'log_order_status';
}
