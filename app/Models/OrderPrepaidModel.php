<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPrepaidModel extends Model
{
    protected $table = 'order_prepaid';
    protected $primaryKey = 'prepaid_id';
}
