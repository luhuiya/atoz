<?php

namespace App\Repositories;

use App\Models\OrderPrepaidModel;

class OrderPrepaidRepository extends BaseRepository
{
	public function getOrder($orderId)
	{
		return OrderPrepaidModel::where('order_id', $orderId)->select([ 
			'prepaid_number', 
			'prepaid_value',
		])->first();
	}

	public function getOrders($orderIds)
	{
		return OrderPrepaidModel::whereIn('order_id', $orderIds)->select([ 
			'order_id',
			'prepaid_number', 
			'prepaid_value',
		])->get();
	}

	public function save($data)
	{
		$row = new OrderPrepaidModel();

        $row->order_id 			= $data['order_id'];
        $row->prepaid_number 	= $data['prepaid_number'];
        $row->prepaid_value 	= $data['prepaid_value'];

        $row->save();
	}
}