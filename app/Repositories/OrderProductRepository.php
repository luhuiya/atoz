<?php

namespace App\Repositories;

use App\Models\OrderProductModel;

class OrderProductRepository extends BaseRepository
{
	public function updateShippingCode($orderId, $code)
	{
		$row = OrderProductModel::where('order_id', $orderId)->first();

		$row->product_shipping_code = $code;
		$row->save();
	}

	public function getOrders($orderIds)
	{
		return OrderProductModel::whereIn('order_id', $orderIds)->select([ 
			'order_id',
			'product_name', 
			'product_shipping_code',
			'product_price',
		])->get();
	}

	public function getOrder($orderId)
	{
		return OrderProductModel::where('order_id', $orderId)->select([ 
			'product_name', 
			'product_shipping_address',
			'product_price',
		])->first();
	}

	public function save($data)
	{
		$row = new OrderProductModel();

        $row->order_id 					= $data['order_id'];
        $row->product_name 				= $data['product_name'];
        $row->product_shipping_address 	= $data['product_shipping_address'];
        $row->product_price 			= $data['product_price'];

        $row->save();
	}
}