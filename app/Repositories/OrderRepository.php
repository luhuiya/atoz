<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Auth;
use Request;

use App\Models\OrderModel;
use App\Models\LogOrderStatusModel;

use Carbon\Carbon;

use App\Config\OrderStatus;

class OrderRepository extends BaseRepository
{
	public function getList($userId, $search, $row = 20)
	{
		$data = OrderModel::where('user_id', $userId);

		if(!empty($search))
		{
			$strlen = strlen($search);
			if($strlen == 10)
			{
				$data = $data->where('order_number', $search);
			}
			else if($strlen < 10)
			{
				$data = $data->where('order_number', 'like', '%'. $search .'%');
			}
		}
		return $data->orderBy('created_at', 'desc')
			->select([
				'order_id',
				'order_number',
				'order_type',
				'order_price',
				'order_status',
				'created_at',
			])	
			->paginate($row);
	}

	public function updateStatus($orderId, $orderStatus)
	{
		$order = OrderModel::find($orderId);
		if($order == null)
		{
			return false;
		}

		$order->order_status = $orderStatus;
		if($orderStatus == OrderStatus::SUCCESS || $orderStatus == OrderStatus::FAIL)
		{
			$order->order_paytime = Carbon::now();
		}

		$order->save();

        $this->insertLog($orderId, $orderStatus);

		return true;
	}

	public function existsNumber($number)
	{
		return OrderModel::where('order_number', $number)->first() != null;
	}

	public function getOrder($number, $fields)
	{
		return OrderModel::where('order_number', $number)->select($fields)->first();
	}

	public function getOrderId($number)
	{
		$row = OrderModel::where('order_number', $number)->select('order_id')->first();
		return $row->order_id;
	}

	public function save($data)
	{
		$row = new OrderModel();

        $row->user_id 		= $data['user_id'];
        $row->order_number 	= $data['order_number'];
        $row->order_price 	= $data['order_price'];
        $row->order_type 	= $data['order_type'];
        $row->order_status 	= $data['order_status'];

        $row->save();

        $orderId = $this->getOrderId($data['order_number']);
        $this->insertLog($orderId, $data['order_status']);

        return $orderId;
	}

	private function insertLog($orderId, $status)
	{
		$row = new LogOrderStatusModel();

        $userId = Auth::user()->id;
        
        $row->user_id 		= $userId;
        $row->order_id 		= $orderId;
        $row->log_status 	= $status;
        $row->ip_address    = Request::ip();

        $row->save();

	}
}