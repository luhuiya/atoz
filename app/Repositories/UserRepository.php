<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Auth;
use Request;

use App\Models\LogLoginModel;

class UserRepository extends BaseRepository
{
	public function insertLog()
	{
		$row = new LogLoginModel();

        $userId = Auth::user()->id;
        
        $row->user_id 		= $userId;
        $row->ip_address    = Request::ip();
        $row->save();

	}
}