<?php

namespace App\Services;

use App\Repositories\OrderRepository;
use App\Services\ProductService;
use App\Services\PrepaidService;

use Illuminate\Support\Facades\Auth;

use App\Helpers\NumberHelper;
use App\Helpers\OrderHelper;
use App\Config\OrderStatus;
use App\Config\OrderType;

class ListOrderService
{
	private $orderRepository;

	public function __construct()
	{
		$this->orderRepository = new OrderRepository();
	}

	private function getData($arr, $orderIds, $type, $instance)
	{
		if(!empty($orderIds[$type]))
		{
			$temp = $orderIds[$type];
			$rows = $instance->getOrders($temp);

			foreach ($rows as $row)
			{
				$arr[$row->order_id] = [
					'text' => $instance->getShortText($row),
					'row' => $row
				];
			}
		}

		return $arr;
	}

	private function getDetail($orderIds)
	{
		$arr = [];
		$arr = $this->getData($arr, $orderIds, OrderType::PREPAID_BALANCE, new PrepaidService());
		$arr = $this->getData($arr, $orderIds, OrderType::PRODUCT_COMMERCE, new ProductService());
		return $arr;
	}

	private function addOrderText($result, $detailData)
	{
		foreach ($result as $row)
		{
			if(!isset($detailData[$row->order_id]))
				continue;

			$detail = $detailData[$row->order_id];
			$row->order_text = $detail['text'];
			if($row->order_status == OrderStatus::SUCCESS && $row->order_type == OrderType::PRODUCT_COMMERCE)
			{
				$detailRow = $detail['row'];
				$row->order_status = 'Shipping Code : ' . $detailRow->product_shipping_code; 
			}
		}
	}

	public function get($search)
	{
		$userId = Auth::user()->id;
		$result = $this->orderRepository->getList($userId, $search);
		$orderIds = [];

		foreach($result as $idx => $row)
		{
			if($row->order_status == OrderStatus::PENDING)
			{
				$isExpired = OrderHelper::isExpired($row->created_at);
				if($isExpired)
				{
					$row->order_status = OrderStatus::CANCELED;
					$this->orderRepository->updateStatus($row->order_id, OrderStatus::CANCELED);
				}
			}

			$row->order_price = NumberHelper::format($row->order_price);
			$orderIds[$row->order_type][] = $row->order_id;
		}
		
		$detailData = $this->getDetail($orderIds);
		$this->addOrderText($result, $detailData);

		return $result;
	}
}