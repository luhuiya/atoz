<?php

namespace App\Services;

use App\Config\OrderStatus;
use App\Repositories\OrderRepository;
use App\Helpers\GenerateHelper;

use Illuminate\Support\Facades\Auth;

abstract class OrderService
{
	private $orderRepository;
	protected $type = ''; 

	abstract protected function getTotalPrice();
	abstract public function createOrder($request);

	public function __construct()
	{
		$this->orderRepository = new OrderRepository();
	}

	private function getOrderNumber()
	{
		$orderNumber = GenerateHelper::getRandomNumber();
		$isExists = $this->orderRepository->existsNumber($orderNumber);
		return $isExists? $this->getOrderNumber() : $orderNumber;
	}

	protected function createOrderRow(){

		$order = [];
		$orderNumber = $this->getOrderNumber();
		$order['user_id'] = Auth::user()->id;
		$order['order_number'] = $orderNumber;
		$order['order_price'] = $this->getTotalPrice();
		$order['order_type'] = $this->type;
		$order['order_status'] = OrderStatus::PENDING;
		
		$order['order_id'] = $this->orderRepository->save($order);
		
		return $order;
	}
}