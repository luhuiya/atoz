<?php

namespace App\Services;

use App\Repositories\OrderRepository;
use App\Repositories\OrderProductRepository;

use Illuminate\Support\Facades\Auth;

use App\Config\OrderStatus;
use App\Config\OrderType;
use App\Config\Message;

use App\Helpers\OrderHelper;
use App\Helpers\DateHelper;
use App\Helpers\GenerateHelper;

class PaymentService
{
	private $orderRepository;

	public function __construct()
	{
		$this->orderRepository = new OrderRepository();
	}

	private function validateOrder($order)
	{
		if($order == NULL)
		{
			return Message::ORDER_NOT_FOUND;
		}

        $userId = Auth::user()->id;
		if($order->user_id != $userId)
		{
			return Message::ORDER_WRONG_USER;
		}

		$status = $order->order_status;
		if($status == OrderStatus::CANCELED || OrderHelper::isExpired($order->created_at))
		{
			//update canceled status
			$this->orderRepository->updateStatus($order->order_id, OrderStatus::CANCELED);
			return Message::ORDER_ALREADY_CANCELED;
		}

		if($status != OrderStatus::PENDING)
		{
			return Message::ORDER_WRONG_STATUS;
		}

		return null;
	}

	private function updatePrepaidToPaid($order)
	{
		//Mark the order is paid and record the paid time.
		// Random the success rate by the hour and mark that the process is failed or success.
		$isDaylight = DateHelper::isDaylight();
		$isSuccess = GenerateHelper::getIsSuccess($isDaylight);

		$orderStatus = $isSuccess == 1 ? OrderStatus::SUCCESS : OrderStatus::FAIL;
		$this->orderRepository->updateStatus($order->order_id, $orderStatus);
	}

	private function updateProductToPaid($order)
	{
		//Mark the order is paid and record the paid time.
		// Generate 8 letters of random character to be the Shipping Code.
		$this->orderRepository->updateStatus($order->order_id, OrderStatus::SUCCESS);

		$orderProductRepository = new OrderProductRepository();
		$orderProductRepository->updateShippingCode($order->order_id, GenerateHelper::getRandomString());
	}

	private function updateToPaid($order)
	{
		switch ($order['order_type'])
		{
			case OrderType::PREPAID_BALANCE:
				return $this->updatePrepaidToPaid($order);
			case OrderType::PRODUCT_COMMERCE:
				return $this->updateProductToPaid($order);
		}
	}

	public function pay($orderNumber)
	{
		$order = $this->orderRepository->getOrder($orderNumber, [ 
			'order_id',
			'order_type',
			'order_status',
			'user_id',
			'created_at'
		]);

		$errors = $this->validateOrder($order);
		if($errors != null)
		{
			return [ 'error' => $errors ];
		}

		$this->updateToPaid($order);
	} 
}