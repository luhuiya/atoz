<?php

namespace App\Services;

use App\Repositories\OrderPrepaidRepository;
use App\Config\OrderType;
use App\Helpers\NumberHelper;

class PrepaidService extends OrderService
{
	private $orderPrepaidRepository;
	private $prepaidValue;

	public function __construct()
	{
		parent::__construct();

		$this->type = OrderType::PREPAID_BALANCE;
		$this->orderPrepaidRepository = new OrderPrepaidRepository();
	}

	public function getText($orderId)
	{
		$row = $this->orderPrepaidRepository->getOrder($orderId);
		return 'Your Mobile Phone Number <b>' . $row['prepaid_number'] . '</b> will be topped up for <b>' . NumberHelper::format($row['prepaid_value']) . '</b> after you pay';
	}

	public function getShortText($row)
	{
		return '<b>' . NumberHelper::format($row->prepaid_value) . '</b> for <b>' . $row->prepaid_number . '</b>';
	}

	public function getOrders($orderIds)
	{
		return $this->orderPrepaidRepository->getOrders($orderIds);
	}

	protected function getTotalPrice()
	{
		 // Prepaid balance : Value + 5% 
		return $this->prepaidValue + ($this->prepaidValue * 5 / 100);
	}

	public function createOrder($request)
	{
		$this->prepaidValue = $request['prepaid_value'];

		$order = $this->createOrderRow();

		$row = [];
		$row['order_id'] 		= $order['order_id'];
		$row['prepaid_number'] 	= $request['prepaid_number'];
		$row['prepaid_value'] 	= $request['prepaid_value'];

		$this->orderPrepaidRepository->save($row);

		return $order['order_number'];
	}
}