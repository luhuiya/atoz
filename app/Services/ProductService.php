<?php

namespace App\Services;

use App\Repositories\OrderProductRepository;
use App\Config\OrderType;
use App\Helpers\NumberHelper;

class ProductService extends OrderService
{
	private $orderProductRepository;
	private $productPrice;

	public function __construct()
	{
		parent::__construct();

		$this->type = OrderType::PRODUCT_COMMERCE;
		$this->orderProductRepository = new OrderProductRepository();
	}

	public function getText($orderId)
	{
		$row = $this->orderProductRepository->getOrder($orderId);
		return '<b>' . $row['product_name'] . '</b> that cost <b>' . NumberHelper::format($row['product_price']) . '</b> will be shipped to <b>'. $row['product_shipping_address'] .'</b> after you pay';
	}

	public function getShortText($row)
	{
		return '<b>' . $row->product_name . '</b> that cost <b>' . NumberHelper::format($row->product_price) . '</b>';
	}

	public function getOrders($orderIds)
	{
		return $this->orderProductRepository->getOrders($orderIds);
	}

	protected function getTotalPrice()
	{
		 // Product commerce : Value + 10.000  
		return $this->productPrice + 10000;
	}

	public function createOrder($request)
	{
		$this->productPrice = $request['product_price'];

		$order = $this->createOrderRow();

		$row = [];
		$row['order_id'] 					= $order['order_id'];
		$row['product_name'] 				= $request['product_name'];
		$row['product_shipping_address'] 	= $request['product_shipping_address'];
		$row['product_price'] 				= $request['product_price'];

		$this->orderProductRepository->save($row);

		return $order['order_number'];
	}
}