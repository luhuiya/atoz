<?php

namespace App\Services;

use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\Auth;
use App\Config\OrderType;
use App\Helpers\NumberHelper;

class SuccessService 
{
	public function __construct()
	{
		$this->orderRepository = new OrderRepository();
	}

	function getText($order)
	{
		switch ($order['order_type'])
		{
			case OrderType::PREPAID_BALANCE:
				$instance = new PrepaidService(); 
				return $instance->getText($order['order_id']);
			case OrderType::PRODUCT_COMMERCE:
				$instance = new ProductService(); 
				return $instance->getText($order['order_id']);
		}

		return '';
	}

	function getData($number)
	{
		$row = $this->orderRepository->getOrder($number, [ 
			'order_number', 
			'order_price',
			'order_type',
			'order_id',
			'user_id',
		]);

        $userId = Auth::user()->id;
		if($row == null || $row->user_id != $userId)
		{
			return [];
		}

		$data = [];

        $data['order_number'] 	= $row->order_number;
        $data['total'] 			= NumberHelper::format($row->order_price);
        $data['text'] 			= $this->getText($row);

        return $data;
	}
}