<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPrepaidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_prepaid', function (Blueprint $table) {
            $table->increments('prepaid_id');
            $table->unsignedInteger('order_id');
            $table->string('prepaid_number');
            $table->double('prepaid_value');
            $table->timestamps();
            $table->foreign('order_id')->references('order_id')->on('order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_prepaid');
    }
}
