@extends('layouts.app')

@section('content')
	<div style="margin: 30px 0; text-align: right;">
	<form action="{{ route('order') }}">
		<input type="text" name="q" value="{{ $search }}" placeholder="search order number here..." maxlength="10" style="width: 200px; padding: 10px;"/>
	</form>
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>Order No.</th>
				<th>Description</th>
				<th>Total</th>
				<th>Information</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($data as $row)
			<tr>
				<td>{{ $row->order_number }}</td>
				<td>{!! $row->order_text !!}</td>
				<td>{{ $row->order_price }}</td>
				<td>
					@if($row->order_status == App\Config\OrderStatus::PENDING)
						<a class="btn btn-primary" href="{{ route('payment.form', [ 'id' => $row->order_number ]) }}">Pay</a>
					@elseif($row->order_status == App\Config\OrderStatus::SUCCESS)
						<span style="color: green">Success</span>
					@elseif($row->order_status == App\Config\OrderStatus::FAIL)
						<span style="color: gray">Fail</span>
					@elseif($row->order_status == App\Config\OrderStatus::CANCELED)
						<span style="color: red">Canceled</span>
					@else
						{{$row->order_status}}
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	<table>
	{{ $data->appends([ 'q' => $search ])->links() }}
@endsection
