@extends('layouts.app')

@section('content')

<form method="POST" action="{{ route('payment.store') }}">
	{{ csrf_field() }}
	<div class="form-group row">
		<label for="order_number" class="col-sm-4 col-form-label">Order Number</label>
		<div class="col-sm-8">
			<input type="text" value="{{ empty($number)? old('order_number') : $number }}" class="form-control" id="order_number" name="order_number">
			@if ($errors->has('order_number'))
				<span class="help-block">
				<strong>{{ $errors->first('order_number') }}</strong>
				</span>
			@endif
		</div>
	</div>
	<div><button class="btn btn-primary" style="width: 100%">Pay</button></div>
</form>
@endsection
