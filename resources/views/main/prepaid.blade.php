@extends('layouts.app')

@section('content')
<form method="POST" action="{{ route('prepaid.store') }}">
	{{ csrf_field() }}
	<div class="form-group row">
		<label for="prepaid_number" class="col-sm-4 col-form-label">Mobile Phone Number</label>
		<div class="col-sm-8">
			<input type="text" value="{{ old('prepaid_number') }}" class="form-control" id="prepaid_number" name="prepaid_number">
			@if ($errors->has('prepaid_number'))
				<span class="help-block">
				<strong>{{ $errors->first('prepaid_number') }}</strong>
				</span>
			@endif
		</div>
	</div>
	<div class="form-group row">
		<label for="prepaid_value" class="col-sm-4 col-form-label">Value</label>
		<div class="col-sm-8">
			<select type="text" value="{{ old('prepaid_value') }}" class="form-control" id="prepaid_value" name="prepaid_value">
				<option value=""> - Select One - </option>
				<option value="10000">10000</option>
				<option value="50000">50000</option>
				<option value="100000">100000</option>
			</select>
			@if ($errors->has('prepaid_value'))
				<span class="help-block">
				<strong>{{ $errors->first('prepaid_value') }}</strong>
				</span>
			@endif
		</div>
	</div>
	<div><button class="btn btn-primary pull-right">Submit</button></div>
</form>
@endsection
