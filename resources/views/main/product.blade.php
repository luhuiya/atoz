@extends('layouts.app')

@section('content')
<form method="POST" action="{{ route('product.store') }}">
  {{ csrf_field() }}
  <div class="form-group row">
    <label for="product_name" class="col-sm-4 col-form-label">Product</label>
    <div class="col-sm-8">
      <textarea type="text" class="form-control" id="product_name" name="product_name">{{ old('product_name') }}</textarea>
      @if ($errors->has('product_name'))
        <span class="help-block">
        <strong>{{ $errors->first('product_name') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="product_shipping_address" class="col-sm-4 col-form-label">Shipping Address</label>
    <div class="col-sm-8">
      <textarea type="text" class="form-control" id="product_shipping_address" name="product_shipping_address">{{ old('product_shipping_address') }}</textarea>
      @if ($errors->has('product_shipping_address'))
        <span class="help-block">
        <strong>{{ $errors->first('product_shipping_address') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="product_price" class="col-sm-4 col-form-label">Price</label>
    <div class="col-sm-8">
      <input type="text" value="{{ old('product_price') }}" class="form-control" id="product_price" name="product_price">
      @if ($errors->has('product_price'))
        <span class="help-block">
        <strong>{{ $errors->first('product_price') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div><button class="btn btn-primary pull-right">Submit</button></div>
</form>
@endsection
