@extends('layouts.app')

@section('content')
<div style="text-align: center;">
	<div style="margin: 30px 0;">
	<div class="row">
		Your Order Number
	</div>
	<div class="row">
		{{ $data['order_number'] }}
	</div>
	</div>

	<div style="margin: 30px 0;">
	<div class="row">
		Total
	</div>
	<div class="row">
		{{ $data['total'] }}
	</div>
	</div>

	<div style="margin: 30px 0;">
		{!! $data['text'] !!}
	</div>

	<a class="btn btn-primary" href="{{ route('payment.form', [ 'id' => $data['order_number'] ]) }}">Pay Here</a>
</div>
@endsection
