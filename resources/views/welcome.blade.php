@extends('layouts.app')

@section('content')
<div style="text-align: center;">
	<h3>Welcome to Atoz.com</h3>
	<a class="btn btn-lg btn-primary" href="{{ route('login') }}">Login</a>
</div>
@endsection
