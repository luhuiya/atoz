<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('product', 'Main\ProductController@index')->name('product.form');
Route::post('product', 'Main\ProductController@store')->name('product.store');

Route::get('prepaid-balance', 'Main\PrepaidController@index')->name('prepaid.form');
Route::post('prepaid-balance', 'Main\PrepaidController@store')->name('prepaid.store');

Route::get('success/{id}', 'Main\SuccessController@index')->name('success');

Route::get('order', 'Main\OrderController@index')->name('order');

Route::get('payment/{id?}', 'Main\PaymentController@index')->name('payment.form');
Route::post('payment', 'Main\PaymentController@store')->name('payment.store');

